// #include <stdio.h>
// int main()
// {
//     int k;
//     for (k = -3; k < -5; k++)
//         printf("Hello");
//     return 0;
// }

// #include <stdio.h>
// int main()
// {
//     int a = 0, i;
//     for (i = 0;i < 5; i++)
//     {
//         a++;
//         continue;
//     }
//     printf("%d %d",i,a);
//     return 0;
// }

// #include <stdio.h>
// int main()
// {
//     int a = 0, i;
//     for (i = 0;i < 5; i++)
//     {
//         continue;
//         a++;
//     }
//     printf("%d %d",i,a);
//     return 0;
// }

// #include <stdio.h>
// int main()
// {
//     int i = 0;
//     while (i = 0) {  //infinite loop 
//         printf("Hello\n");
//     }
//     return 0;
// }

/*Which of the below loop will give the same values of i in reverse order for the loop 
for(int i=0;i<n;i++)  ?*/
// #include<stdio.h>
// int main(){

// int sum = 0;
// int i,n;
//     scanf("%d",&n);
//     for (i=n-1; i>=0; i--)
//     {
//         sum+=i;
//     }
//     printf("Total:%d", sum);

//     return 0;
// }

/**
 * Problem Statement : From HackerRank
You will be given a positive integer N, you need to print from 1 to N and besides the value, print Yes or No. Print Yes if the value is divisible by 5 and print No otherwise.
* Input given : 10
* Output : 
        1 No
        2 No
        3 No
        4 No
        5 Yes
        6 No
        7 No
        8 No
        9 No
        10 Yes
*/
// #include <stdio.h>

// int main() {

//     int i, N;
//     scanf("%d", &N);
//     for (i=1; i<=N; i++)
//     {
//         if(i%5==0)
//         {
//             printf("%d Yes\n", i);
//         }
//         else{
//             printf("%d No\n", i);
//         }
//     }
//     return 0;
// }